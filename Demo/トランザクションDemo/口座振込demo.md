

### Demo口座振替

##### JDBC - Apache Druid

- Druidとは

  主流のデータベースプールの一つである、他にDBCP(Tomcat)、C3P0(Springフレームワーク)があります。DruidはAlibabaが開発しました、時系列データ解析は特徴。

- 今回の例

  DruidDataSourceFactoryを使ってDatasourceを生成しました。

  ```Java
  import com.alibaba.druid.pool.DruidDataSourceFactory;
  
  import javax.sql.DataSource;
  import java.io.InputStream;
  import java.sql.Connection;
  import java.sql.SQLException;
  import java.util.Properties;
  
  /**
   * @Author jiang.weiyu
   * @Date 2020/12/18 16:56
   * Druidによる接続プールの設定
   */
  public class Druid_Utils {
  
      public Druid_Utils() {
      }
  
      // 接続プールの生成;
      private static DataSource dataSource;
  
      static {
          Properties properties = new Properties();
          // class loader方式から接続プロパティの取得
          InputStream resourceAsStream = Druid_Utils.class.getClassLoader().getResourceAsStream("druid_config.properties");
          try {
              properties.load(resourceAsStream);
              dataSource = DruidDataSourceFactory.createDataSource(properties);
          } catch (Exception e) {
              e.printStackTrace();
          }
      }
  
      public static DataSource getDataSource() {
          return dataSource;
      }
      
      // 接続する
      public static Connection getConnection() {
          try {
              return dataSource.getConnection();
          } catch (SQLException e) {
              return null;
          }
      }
  
      // 資源の解放
      public static void close(Connection connection) throws SQLException {
          if (null != connection) connection.close();
      }
  }
  ```

##### DBUtils

- DbUtilsとは
  Apache Commons DbUtilsというJDBCをパッケージ化したAPIは従来のJDBCの処理をシンプル化しました。

- DbUtilsのCore機能

  1. QueryRunner - SQL分(SELECT,UPDATE,DELETE)を処理するAPI

  2. ResultSetHandler - インスタンスである、実装したインスタンスはしたに

     ScalarHandler<T> - 第一行目の結果を取得し、対象オブジェクに入れる

     BeanHandler<>(T.class)  - 取得したデータをJavaBeanに入れる

     ArrayListHandler - 複数のデータをリストに保存

     MapListHandler - 複数のデータをまずkey,valueの形でMapに入れ、後Listに保存する

  3. DbUtilsクラス - 事務のBEGIN,Commit,Rollbackを処理する機能

- 今回の例はこちらになります

  ```Java
  import org.apache.commons.dbutils.QueryRunner;
  import org.apache.commons.dbutils.handlers.ScalarHandler;
  import www.lagou.entity.TransactionBean;
  import www.lagou.utils.Druid_Utils;
  
  import java.sql.Connection;
  import java.sql.SQLException;
  import java.sql.Timestamp;
  import java.util.Date;
  import java.util.Scanner;
  
  /**
   * @Author jiang.weiyu
   * @Date 2020/12/18 17:43
   * 振込テスト
   */
  public class TradeTest {
      public static void main(String[] args) {
          Scanner sc = new Scanner(System.in);
          System.out.println("あなたの名前を入力してください");
          String name = sc.next();
          System.out.println("あなたのカードナンバーを入力してください");
          String id = sc.next();
          Object[] prams = {id, name};
  
          System.out.println("振込先のカードナンバーを入力してください");
          String id2 = sc.next();
          System.out.println("振込金額を入力してください");
          double money = sc.nextDouble();
          // DBUtilsオブジェクを作成
          QueryRunner queryRunner = new QueryRunner();
          // コネクションを取得
          Connection con = Druid_Utils.getConnection();
          // 取引した時間
          Timestamp ts = new Timestamp(new Date().getTime());
          try {
              // トランザクションをStart
              System.out.println("処理始まる...");
              con.setAutoCommit(false);
  
              //　残高を確認し、不足の場合取引中止
              String sql = "select balance from account where card = ? and username = ?";
              double balance = queryRunner.query(con, sql, new ScalarHandler<>(), prams);
  
              if (balance < money) throw new RuntimeException("残高不足...");
  
              String sql1 = "update account set balance = (balance - ?) where card = ?";
              String sql2 = "update account set balance = (balance + ?) where card = ?";
  
              Object[] obj1 = {money, id};
              Object[] obj2 = {money, id2};
  
              queryRunner.update(con, sql1, obj1);
              Timestamp time1 = new Timestamp(new Date().getTime());
  
              queryRunner.update(con, sql2, obj2);
              Timestamp time2 = new Timestamp(new Date().getTime());
  
              TransactionBean t1 = new TransactionBean(id,"送金",money,time1);
              TransactionBean t2 = new TransactionBean(id,"着金",money,time2);
              // 写入交易记录
              System.out.println("取引内容を記録します...");
              String sql3 = "INSERT INTO TRANSACTION (cardid,tratype,tramoney,tradate) VALUES(?,?,?,?)";
              Object[] o1 = {t1.getCardID(), t1.getTraType(),t1.getTraMoney(),t1.getTimestamp()};
              Object[] o2 = {t2.getCardID(), t2.getTraType(),t2.getTraMoney(),t2.getTimestamp()};
  
              queryRunner.update(con,sql3,o1);
              queryRunner.update(con,sql3,o2);
  
              con.commit();
              System.out.println("取引完了");
  
          } catch (SQLException e) {
              try {
                  con.rollback();
              } catch (SQLException e1) {
                  e1.printStackTrace();
              }
          } finally {
              try {
                  con.close();
              } catch (SQLException e) {
                  e.printStackTrace();
              }
              sc.close();
          }
      }
  }
  ```

##### Properties

- ```properties
  driverClassName=com.mysql.jdbc.Driver
  url=jdbc:mysql://47.74.5.215:3306/test01?characterEncoding=UTF-8
  username=testUser
  password=12345678
  initialSize=5
  maxActive=10
  maxWait=3000
  ```



##### 処理結果

- 成功の場合　00:45分の取引内容は記録されました

  ![task1截图记录了12点45分的一笔交易](https://gitee.com/jiangweiyu/typora-pic/raw/master/20201221005557.jpg)

- 失敗の場合

  1/0をソースコードにいれ、実行エラーになったが、トランザクションを始まったため、エラー時rollbackし、DBの金額は変換しません

  ![task01交易异常终止时由于开启了事务账户数据不会更新](https://gitee.com/jiangweiyu/typora-pic/raw/master/20201221005704.jpg)
